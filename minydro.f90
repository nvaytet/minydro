!###############################################################################
! MINYDRO: Minimal Cell-Based Hydrodynamics
! Written by Neil Vaytet (ENS Lyon)
! Version 1.0 - July 2013
! Compile with: gfortran -o minydro minydro.f90 -ffree-line-length-none
! Add the -fopenmp flag if you wish to use OpenMP parallelisation.
!###############################################################################

!###############################################################################

module parameters
  implicit none
  integer, parameter :: dp   = 8
  integer, parameter :: nvar = 5
end module parameters

!###############################################################################

module cell_structure
  use parameters
  implicit none
  type cell
     integer , dimension(6) :: neighbour
     real(dp)               :: density
     real(dp), dimension(3) :: momentum
     real(dp), dimension(3) :: x_centre
     real(dp), dimension(3) :: x_left
     real(dp), dimension(3) :: x_right
     real(dp), dimension(5) :: update
     real(dp), dimension(6) :: area
     real(dp)               :: energy
     real(dp)               :: volume
     logical , dimension(6) :: box_edge
  end type cell
end module cell_structure

!###############################################################################

module grid_structure
  use cell_structure
  implicit none
  integer                                 :: ndim
  integer                                 :: ncells
  integer   , dimension(3  )              :: nx
  integer   , dimension(3  )              :: iodd
  integer   , dimension(3  )              :: ieven
  integer   , dimension(5,3)              :: ivar
  integer   , dimension(5  )              :: icycle
  integer   , dimension(6  )              :: boundaries
  real(dp)  , dimension(3  )              :: lbox
  type(cell), dimension(:  ), allocatable :: grid_cells
end module grid_structure

!###############################################################################

module variables
  use parameters
  implicit none
  real(dp)                           :: gam
  real(dp)                           :: gm1
  real(dp)                           :: dt
  real(dp)                           :: time
  real(dp)                           :: time_limit
  real(dp)                           :: cfl = 0.2_dp
  logical                            :: use_slopes
  integer                            :: ncolors
  integer, dimension(:), allocatable :: red
  integer, dimension(:), allocatable :: grn
  integer, dimension(:), allocatable :: blu
end module variables

!###############################################################################

module constants
  use parameters
  implicit none
  real(dp), parameter :: zero         = 0.00e+00_dp  !<  0
  real(dp), parameter :: one          = 1.00e+00_dp  !<  1
  real(dp), parameter :: two          = 2.00e+00_dp  !<  2
  real(dp), parameter :: half         = 0.50e+00_dp  !< 1/2
  real(dp), parameter :: large_number = 1.00e+30_dp  !< A very large number
end module constants

!###############################################################################

program minydro

  use variables
  use constants

  implicit none
  
  logical :: loop,writetxt,writeppm,writevtk,writebin
  integer :: it,iout,nout
  
  write(*,'(a)') '########################'
  write(*,'(a)') '        MINYDRO         '
  write(*,'(a)') ' Written by Neil Vaytet '
  write(*,'(a)') '     (version 1.0)      '
  write(*,'(a)') '########################'
  write(*,*)
  
  ! Parameters (can be changed by the user)
  nout       = 100     ! Write output every nout timesteps
  time_limit = 1.0_dp  ! Simulation time limit
  use_slopes = .true.  ! Use second order reconstruction?
  
  ! Other variables
  loop       = .true.
  iout       = 0
  it         = 0
  time       = zero
  writetxt   = .false.
  writeppm   = .true.
  writevtk   = .false.
  writebin   = .false.
  
  call setup
  
  call output(iout,writetxt,writeppm,writevtk,writebin)

  do while(loop)
  
     it = it + 1
  
     call compute_timestep
  
     call compute_hydro
     
     time = time + dt
     
     write(*,'(a,i6,a,es10.3,a,es10.3)') 'it = ',it,'  time = ',time,'  dt = ',dt
     
     if(mod(it,nout) == 0)then
        iout = iout + 1
        call output(iout,writetxt,writeppm,writevtk,writebin)
     endif
     
     if(time >= time_limit) loop = .false.
  
  enddo
  
  iout = iout + 1
  call output(iout,writetxt,writeppm,writevtk,writebin)

  stop

end program minydro

!###############################################################################

subroutine setup

  use cell_structure
  use grid_structure
  use variables
  use constants

  implicit none
  
  real(dp), dimension(3) :: dx
  real(dp)               :: dist,rho1,p1,p2,kinetic_energy,gradient,offset
  integer                :: i,n,k,j

  ! Constants
  gam = 1.4_dp
  gm1 = gam - one
  
  ! Grid dimensions
  ndim  = 2
  nx(1) = 256
  nx(2) = 384
  nx(3) = 1
  
  ! Simulation box size
  lbox(1) = 1.0_dp
  lbox(2) = 1.5_dp
  lbox(3) = 1.0_dp
  
  ! Boundary conditions: 0 = free-flow, 1 = periodic
  boundaries = 1
  
  ! Generate cell mesh
  call generate_cells

  ! Update cell coordinates
  do k = 1,ndim
     dx(k) = lbox(k) / real(nx(k),dp)
  enddo
  
  do n = 1,ncells
     do k = 1,ndim
  
        if(grid_cells(n)%box_edge(iodd(k)))then
           grid_cells(n)%x_left  (k) = zero - half * lbox(k) ! defines the cell coordinates
           grid_cells(n)%x_right (k) = grid_cells(n)%x_left(k) + dx(k)
           grid_cells(n)%x_centre(k) = half * (grid_cells(n)%x_left(k) + grid_cells(n)%x_right(k))
        endif
        
     enddo
  enddo
  
  do n = 1,ncells
     do k = 1,ndim
     
        if(.not. grid_cells(n)%box_edge(iodd(k)))then
           i = grid_cells(n)%neighbour(iodd(k))
           grid_cells(n)%x_left  (k) = grid_cells(i)%x_right(k)
           grid_cells(n)%x_right (k) = grid_cells(n)%x_left(k) + dx(k)
           grid_cells(n)%x_centre(k) = half * (grid_cells(n)%x_left(k) + grid_cells(n)%x_right(k))
        endif
        
     enddo
  enddo
  
  ! Compute cell face areas and volume
  do n = 1,ncells
     grid_cells(n)%area   = one
     grid_cells(n)%volume = one
     do k = 1,ndim
        do j = 1,ndim-1
           grid_cells(n)%area(iodd (k)) = grid_cells(n)%area(iodd (k)) * (grid_cells(n)%x_right(icycle(k+j)) - grid_cells(n)%x_left(icycle(k+j)))
           grid_cells(n)%area(ieven(k)) = grid_cells(n)%area(ieven(k)) * (grid_cells(n)%x_right(icycle(k+j)) - grid_cells(n)%x_left(icycle(k+j)))
        enddo
        grid_cells(n)%volume = grid_cells(n)%volume * (grid_cells(n)%x_right(k) - grid_cells(n)%x_left(k))
     enddo
  enddo
           
  ! Define conservative variables inside cells
  rho1 =  1.0_dp
  p1   =  1.0_dp
  p2   = 10.0_dp
  
  do n = 1,ncells
  
     ! Conservative variables
     grid_cells(n)%density  = rho1
     grid_cells(n)%momentum = zero
     
     dist = zero
     do k = 1,ndim
        dist = dist + grid_cells(n)%x_centre(k)**2
     enddo
     dist = sqrt(dist)
     if(dist .le. 0.1_dp)then
        grid_cells(n)%energy = p2 / gm1
     else
        grid_cells(n)%energy = p1 / gm1
     endif
     
     ! Update total energy
     kinetic_energy = zero
     do k = 1,ndim
        kinetic_energy = kinetic_energy + grid_cells(n)%momentum(k)*grid_cells(n)%momentum(k)
     enddo
     kinetic_energy = half * kinetic_energy / grid_cells(n)%density
     
     grid_cells(n)%energy = grid_cells(n)%energy + kinetic_energy
     
     ! Make sure update is zero for first timestep
     grid_cells(n)%update = zero
     
  enddo
  
  ! Create colormap
  ncolors = 255
  allocate(red(ncolors),grn(ncolors),blu(ncolors))
  gradient = 2.6265_dp ; offset = 0.0318_dp
  do i = 1,ncolors
     red(i) = min(max(nint(gradient*real(i-1,dp) + ncolors*(offset       )),0),ncolors)
     grn(i) = min(max(nint(gradient*real(i-1,dp) + ncolors*(offset-1.0_dp)),0),ncolors)
     blu(i) = min(max(nint(gradient*real(i-1,dp) + ncolors*(offset-2.0_dp)),0),ncolors)
  enddo
     
  return

end subroutine setup

!###############################################################################

subroutine generate_cells

  use cell_structure
  use grid_structure

  implicit none
  
  integer :: n,k,nmod
  
  ! Count the number of cells
  ncells = 1
  do n = 1,ndim
     ncells = ncells * nx(n)
  enddo
  
  ! Allocate memory space
  allocate(grid_cells(ncells))
  
  ! Define cell ids
  do n = 1,ncells
     grid_cells(n)%box_edge = .false.
  enddo
  
  ! Find cell neighbours
  do n = 1,ncells
  
     grid_cells(n)%neighbour(1) = n - 1
     grid_cells(n)%neighbour(2) = n + 1     
     grid_cells(n)%neighbour(3) = n - nx(1)
     grid_cells(n)%neighbour(4) = n + nx(1)
     grid_cells(n)%neighbour(5) = n - nx(1)*nx(2)
     grid_cells(n)%neighbour(6) = n + nx(1)*nx(2)     
     
     if(nmod(n,nx(1)            ) .eq. 1                    )then
        grid_cells(n)%neighbour(1) = n -             (1-nx(1))*boundaries(1)
        grid_cells(n)%box_edge(1) = .true.
     endif
     if(nmod(n,nx(1)            ) .eq. nx(1)                )then
        grid_cells(n)%neighbour(2) = n +             (1-nx(1))*boundaries(2)
        grid_cells(n)%box_edge(2) = .true.
     endif
     if(nmod(n,nx(1)*nx(2)      ) .le. nx(1)                )then
        grid_cells(n)%neighbour(3) = n - nx(1)*      (1-nx(2))*boundaries(3)
        grid_cells(n)%box_edge(3) = .true.
     endif
     if(nmod(n,nx(1)*nx(2)      ) .gt. nx(1)      *(nx(2)-1))then
        grid_cells(n)%neighbour(4) = n + nx(1)*      (1-nx(2))*boundaries(4)
        grid_cells(n)%box_edge(4) = .true.
     endif
     if(nmod(n,nx(1)*nx(2)*nx(3)) .le. nx(1)*nx(2)          )then
        grid_cells(n)%neighbour(5) = n - nx(1)*nx(2)*(1-nx(3))*boundaries(5)
        grid_cells(n)%box_edge(5) = .true.
     endif
     if(nmod(n,nx(1)*nx(2)*nx(3)) .gt. nx(1)*nx(2)*(nx(3)-1))then
        grid_cells(n)%neighbour(6) = n + nx(1)*nx(2)*(1-nx(3))*boundaries(6)
        grid_cells(n)%box_edge(6) = .true.
     endif
     
  enddo
  
  ! Initialise cyclic arrays
  do k = 1,ndim
     iodd (k) = 1+(k-1)*2
     ieven(k) = k*2
  enddo
  
  do k = 1,2*ndim-1
     icycle(k) = nmod(k,ndim)
  enddo
  
  ! Ivar matrix for flux indices
  ivar(1,1) = 1 ; ivar(1,2) = 1 ; ivar(1,3) = 1
  ivar(2,1) = 2 ; ivar(2,2) = 3 ; ivar(2,3) = 4
  ivar(3,1) = 3 ; ivar(3,2) = 4 ; ivar(3,3) = 2
  ivar(4,1) = 4 ; ivar(4,2) = 2 ; ivar(4,3) = 3
  ivar(5,1) = 5 ; ivar(5,2) = 5 ; ivar(5,3) = 5
  
  return
  
end subroutine generate_cells

!###############################################################################

subroutine output(iout,writetxt,writeppm,writevtk,writebin)

  use grid_structure
  use variables

  implicit none
  
  integer, intent(in)            :: iout
  logical, intent(in)            :: writetxt,writeppm,writevtk,writebin
  character (len=500)            :: string
  character (len=15 )            :: fname
  character (len=9  )            :: offset
  character (len=2  )            :: prec_string
  character (len=1  ), parameter :: end_rec = char(10)    ! end-character for binary-record finalize
  integer                        :: n,k,npoints,cell_type,nvertices,funit,icol
  integer                        :: nbytes_xyz,nbytes_cellc,nbytes_cello,nbytes_cellt,nbytes_dens,nbytes_pres,nbytes_velo
  integer                        :: ioff0,ioff1,ioff2,ioff3,ioff4,ioff5,ioff6,vert_offset_count
  real(dp)                       :: dmin = 0.1,dmax = 1.5
  
  ! Output number 1: TXT file ==================================================
  
  if(writetxt)then
     write(fname,'(a,i4.4,a)') 'output-',iout,'.txt'
     funit = 21
     open (funit,file=fname,form='formatted',status='replace')
     write(funit,'(a1,a8,a16,a16,a16,a16,a16,a16,a16)') '#','x','y','z','rho','ux','uy','uz','E'
     do n = 1,ncells
        write(funit,'(8(es16.8))') grid_cells(n)%x_centre(1),grid_cells(n)%x_centre(2),grid_cells(n)%x_centre(3),&
                                 & grid_cells(n)%density,grid_cells(n)%momentum(1)/grid_cells(n)%density,&
                                 & grid_cells(n)%momentum(2)/grid_cells(n)%density,grid_cells(n)%momentum(3)/grid_cells(n)%density,&
                                 & grid_cells(n)%energy
     enddo
     close(funit)
  endif
  
  ! Output number 2: PPM image =================================================
  if(writeppm)then
     write(fname,'(a,i4.4,a)') 'output-',iout,'.ppm'
     funit = 22
     open (funit,file=fname,status='replace')
     write(funit, '(a2)') 'P6'
     write(funit, '(i0,'' '',i0)') nx(1),nx(2)
     write(funit, '(i0)') ncolors
     do n = 1,ncells
        icol = min(max(int(((grid_cells(n)%density-dmin)/(dmax-dmin))*(ncolors-1))+1,1),ncolors)
        write(funit,'(3a1)',advance='no') achar(red(icol)),achar(grn(icol)),achar(blu(icol))
     enddo
     close(funit)
  endif
  
  ! Output number 3: VTK file ==================================================
  
  if(writevtk .and. (ndim > 1))then
  
     ! Compute data byte sizes and offsets
     npoints   = (2**ndim)
     cell_type = (ndim + 1) * 3
     nvertices = ncells * npoints
     
     nbytes_xyz   = 3 * nvertices * dp
     nbytes_cellc =     nvertices * 4
     nbytes_cello =     ncells    * 4
     nbytes_cellt =     ncells    * 4
     nbytes_dens  =     ncells    * dp
     nbytes_pres  =     ncells    * dp
     nbytes_velo  = 3 * ncells    * dp
     
     ioff0 = 0                                ! xyz coordinates
     ioff1 = ioff0 + sizeof(n) + nbytes_xyz   ! cell connectivity
     ioff2 = ioff1 + sizeof(n) + nbytes_cellc ! cell offsets
     ioff3 = ioff2 + sizeof(n) + nbytes_cello ! cell types
     ioff4 = ioff3 + sizeof(n) + nbytes_cellt ! density
     ioff5 = ioff4 + sizeof(n) + nbytes_dens  ! pressure
     ioff6 = ioff5 + sizeof(n) + nbytes_pres  ! velocity
       
     ! Start writing VTK file
     write(fname,'(a,i4.4,a)') 'output-',iout,'.vtu'
     write(prec_string,'(i2)') dp*8
     funit = 23
     write(*,*) 'Writing VTK file: '//trim(fname)
     open (funit,file=trim(fname),form='unformatted',access='stream',status='replace')
     write(funit) '<?xml version="1.0"?>'//end_rec
     write(funit) '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">'//end_rec
     write(funit) '   <UnstructuredGrid>'//end_rec
     write(string,'(a,i9,a,i9,a)') '   <Piece NumberOfPoints="',nvertices,'" NumberOfCells="',ncells,'">'
     write(funit) trim(string)//end_rec
     write(funit) '      <Points>'//end_rec
     write(offset,'(i9)') ioff0
     write(funit) '         <DataArray type="Float'//prec_string//&
     &'" Name="Coordinates" NumberOfComponents="3" format="appended" offset="'//offset//'" />'//end_rec
     write(funit) '      </Points>'//end_rec
     write(funit) '      <Cells>'//end_rec
     write(offset,'(i9)') ioff1
     write(funit) '         <DataArray type="Int32" Name="connectivity" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(offset,'(i9)') ioff2
     write(funit) '         <DataArray type="Int32" Name="offsets" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(offset,'(i9)') ioff3
     write(funit) '         <DataArray type="Int32" Name="types" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(funit) '      </Cells>'//end_rec
     write(funit) '      <CellData>'//end_rec
     write(offset,'(i9)') ioff4
     write(funit) '         <DataArray type="Float'//prec_string//'" Name="Density" format="appended" offset="'//offset//'" />'//end_rec
     write(offset,'(i9)') ioff5
     write(funit) '         <DataArray type="Float'//prec_string//'" Name="Pressure" format="appended" offset="'//offset//'" />'//end_rec
     write(offset,'(i9)') ioff6
     write(funit) '         <DataArray type="Float'//prec_string//'" Name="Velocity" NumberOfComponents="3" fformat="appended" offset="'//offset//'" />'//end_rec
     write(funit) '      </CellData>'//end_rec
     write(funit) '   </Piece>'//end_rec
     write(funit) '   </UnstructuredGrid>'//end_rec
     write(funit) '   <AppendedData encoding="raw">'//end_rec
     write(funit) '_'
     write(funit) nbytes_xyz
     do n = 1,ncells
        write(funit) grid_cells(n)%x_left (1),grid_cells(n)%x_left (2),grid_cells(n)%x_left(3)
        write(funit) grid_cells(n)%x_right(1),grid_cells(n)%x_left (2),grid_cells(n)%x_left(3)
        write(funit) grid_cells(n)%x_right(1),grid_cells(n)%x_right(2),grid_cells(n)%x_left(3)
        write(funit) grid_cells(n)%x_left (1),grid_cells(n)%x_right(2),grid_cells(n)%x_left(3)
        if(ndim == 3)then
           write(funit) grid_cells(n)%x_left (1),grid_cells(n)%x_left (2),grid_cells(n)%x_right(3)
           write(funit) grid_cells(n)%x_right(1),grid_cells(n)%x_left (2),grid_cells(n)%x_right(3)
           write(funit) grid_cells(n)%x_right(1),grid_cells(n)%x_right(2),grid_cells(n)%x_right(3)
           write(funit) grid_cells(n)%x_left (1),grid_cells(n)%x_right(2),grid_cells(n)%x_right(3)
        endif
     enddo
     write(funit) nbytes_cellc
     write(funit) (((n-1)*npoints+k-1,k=1,npoints),n=1,ncells)
     write(funit) nbytes_cello
     write(funit) (n*npoints,n=1,ncells)
     write(funit) nbytes_cellt
     write(funit) (cell_type,n=1,ncells)
     write(funit) nbytes_dens
     write(funit) (grid_cells(n)%density,n=1,ncells)
     write(funit) nbytes_pres
     write(funit) (grid_cells(n)%energy,n=1,ncells)
     write(funit) nbytes_velo
     write(funit) ((grid_cells(n)%momentum(k)/grid_cells(n)%density,k=1,3),n=1,ncells)
     write(funit) '   </AppendedData>'//end_rec
     write(funit) '</VTKFile>'//end_rec
     close(funit)

  endif
  
  ! Output number 4: BIN file ==================================================
  if(writebin)then
     funit = 24
     open (funit,file=fname,form='unformatted',status='replace')
     write(funit) ncells
     write(funit) ndim
     write(funit) nx
     write(funit) lbox
     write(funit) time
     write(funit) grid_cells
     close(funit)
  endif
  
  return

end subroutine output

!###############################################################################

subroutine compute_timestep

  use grid_structure
  use constants
  use variables

  implicit none

  integer  :: n,k
  real(dp) :: soundspeed,wavespeed,dt_cell,kinetic_energy,internal_energy
  
  dt = large_number
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(n,k,soundspeed,wavespeed,dt_cell,kinetic_energy,&
  !$OMP internal_energy) REDUCTION(MIN:dt)
  !$OMP DO
  do n = 1,ncells
  
     ! Update new state with conservative update
     grid_cells(n)%density = grid_cells(n)%density + grid_cells(n)%update(1)
     do k = 1,ndim
        grid_cells(n)%momentum(k) = grid_cells(n)%momentum(k) + grid_cells(n)%update(k+1)
     enddo
     grid_cells(n)%energy = grid_cells(n)%energy + grid_cells(n)%update(5)

     ! Compute sound speed inside cell
     kinetic_energy = zero
     do k = 1,ndim
        kinetic_energy = kinetic_energy + grid_cells(n)%momentum(k)*grid_cells(n)%momentum(k)
     enddo
     kinetic_energy = half * kinetic_energy / grid_cells(n)%density
     
     internal_energy = grid_cells(n)%energy - kinetic_energy
  
     soundspeed = sqrt( gam * gm1 * internal_energy / grid_cells(n)%density )
  
     do k = 1,ndim
        wavespeed = abs(grid_cells(n)%momentum(k))/grid_cells(n)%density + soundspeed
        dt_cell = (grid_cells(n)%x_right(k) - grid_cells(n)%x_left(k)) / wavespeed
        dt = min(dt,dt_cell)
     enddo

  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  
  ! CFL condition
  dt = cfl * dt

  return

end subroutine compute_timestep

!###############################################################################

subroutine compute_hydro

  use grid_structure
  use constants
  use variables
  
  implicit none
  
  integer                   :: n,k,h
  real(dp), dimension(nvar) :: var_left,var_centre,var_right,flux_left,flux_right
  real(dp), dimension(nvar) :: var_left_left,var_right_right
  real(dp), dimension(nvar) :: slope_left,slope_centre,slope_right
  real(dp), dimension(nvar) :: state1,state2,state3,state4
  real(dp)                  :: dx1,dx2,slope1,slope2,minmod
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(n,k,h,var_left_left,var_left,var_centre,var_right,&
  !$OMP var_right_right,slope_left,slope_centre,slope_right,dx1,dx2,slope1,slope2,state1,  &
  !$OMP state2,state3,state4,flux_left,flux_right)
  !$OMP DO
  do n = 1,ncells

     grid_cells(n)%update = zero
  
     do k = 1,ndim
     
        ! Left cell
        var_left(1) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%density
        var_left(2) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%momentum(ivar(2,k)-1)
        var_left(3) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%momentum(ivar(3,k)-1)
        var_left(4) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%momentum(ivar(4,k)-1)
        var_left(5) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%energy
        
        ! Centre cell
        var_centre(1) = grid_cells(n)%density
        var_centre(2) = grid_cells(n)%momentum(ivar(2,k)-1)
        var_centre(3) = grid_cells(n)%momentum(ivar(3,k)-1)
        var_centre(4) = grid_cells(n)%momentum(ivar(4,k)-1)
        var_centre(5) = grid_cells(n)%energy
        
        ! Right cell
        var_right(1) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%density
        var_right(2) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%momentum(ivar(2,k)-1)
        var_right(3) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%momentum(ivar(3,k)-1)
        var_right(4) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%momentum(ivar(4,k)-1)
        var_right(5) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%energy
        
        ! Slopes
        slope_left   = zero
        slope_centre = zero
        slope_right  = zero
        
        if(use_slopes)then
        
           ! Left cell
           var_left_left(1) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%density
           var_left_left(2) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%momentum(ivar(2,k)-1)
           var_left_left(3) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%momentum(ivar(3,k)-1)
           var_left_left(4) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%momentum(ivar(4,k)-1)
           var_left_left(5) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%energy
           
           dx1 = grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_centre(k) - grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%x_centre(k)
           dx2 = grid_cells(n)%x_centre(k)                                - grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_centre(k)
           if(dx1*dx2 .ne. zero)then
              do h = 1,nvar
                 slope1 = ( var_left  (h) - var_left_left(h) ) / dx1
                 slope2 = ( var_centre(h) - var_left     (h) ) / dx2
                 slope_left(h) = minmod(slope1,slope2)
              enddo
           endif
        
           ! Centre cell
           dx1 = grid_cells(n)%x_centre(k)                                 - grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_centre(k)
           dx2 = grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_centre(k) - grid_cells(n)%x_centre(k)
           if(dx1*dx2 .ne. zero)then
              do h = 1,nvar
                 slope1 = ( var_centre(h) - var_left  (h) ) / dx1
                 slope2 = ( var_right (h) - var_centre(h) ) / dx2
                 slope_centre(h) = minmod(slope1,slope2)
              enddo
           endif
           
           ! Right cell
           var_right_right(1) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%density
           var_right_right(2) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%momentum(ivar(2,k)-1)
           var_right_right(3) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%momentum(ivar(3,k)-1)
           var_right_right(4) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%momentum(ivar(4,k)-1)
           var_right_right(5) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%energy
           
           dx1 = grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_centre(k)                                 - grid_cells(n)%x_centre(k)
           dx2 = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%x_centre(k) - grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_centre(k)
           if(dx1*dx2 .ne. zero)then
              do h = 1,nvar
                 slope1 = ( var_right      (h) - var_centre(h) ) / dx1
                 slope2 = ( var_right_right(h) - var_right (h) ) / dx2
                 slope_right(h) = minmod(slope1,slope2)
              enddo
           endif
           
        endif
           
        do h = 1,nvar
           state1(h) = var_left  (h) + slope_left  (h) * ( grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_right(k) - grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_centre(k) )
           state2(h) = var_centre(h) - slope_centre(h) * ( grid_cells(n)%x_centre(k)                               - grid_cells(n)%x_left(k)                                  )
           state3(h) = var_centre(h) + slope_centre(h) * ( grid_cells(n)%x_right(k)                                  - grid_cells(n)%x_centre(k)                               )
           state4(h) = var_right (h) - slope_right (h) * ( grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_centre(k) - grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_left(k) )
        enddo
        
        call riemann_solver(state1,state2,flux_left )
        call riemann_solver(state3,state4,flux_right)
        
        do h = 1,nvar
           grid_cells(n)%update(ivar(h,k)) = grid_cells(n)%update(ivar(h,k)) + grid_cells(n)%area(iodd(k))*flux_left(h) - grid_cells(n)%area(ieven(k))*flux_right(h)
        enddo
        
     enddo
     
     ! Store conservative update
     grid_cells(n)%update = grid_cells(n)%update * dt / grid_cells(n)%volume
     
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  
  return

end subroutine compute_hydro

!###############################################################################

subroutine riemann_solver(ucl,ucr,flux)

  use constants
  use variables

  implicit none
  
  ! Arguments
  real(dp), dimension(nvar), intent(in ) :: ucl,ucr
  real(dp), dimension(nvar), intent(out) :: flux
  
  ! Local variables
  real(dp) :: dl,pl,ul,vl,wl,el,dr,pr,ur,vr,wr,er
  real(dp) :: cfastl,rcl,dstarl,cfastr,rcr,dstarr
  real(dp) :: estarl,estarr,ustar,pstar
  real(dp) :: dd,uu,pp,ee,sl,sr
  
  ! Convert from conservative to primitive variables
  dl = ucl(1)
  ul = ucl(2) / ucl(1)
  vl = ucl(3) / ucl(1)
  wl = ucl(4) / ucl(1)
  el = ucl(5)
  pl = ( ucl(5) - half*(ucl(2)*ucl(2)+ucl(3)*ucl(3)+ucl(4)*ucl(4))/ucl(1) ) * gm1
  
  dr = ucr(1)
  ur = ucr(2) / ucr(1)
  vr = ucr(3) / ucr(1)
  wr = ucr(4) / ucr(1)
  er = ucr(5)
  pr = ( ucr(5) - half*(ucr(2)*ucr(2)+ucr(3)*ucr(3)+ucr(4)*ucr(4))/ucr(1) ) * gm1
  
  ! Find the largest eigenvalues in the normal direction to the interface
  cfastl = sqrt(gam*pl/dl)
  cfastr = sqrt(gam*pr/dr)
  
  ! Compute HLL wave speed
  sl = min(ul,ur) - max(cfastl,cfastr)
  sr = max(ul,ur) + max(cfastl,cfastr)
  
  ! Compute lagrangian sound speed
  rcl = dl * (ul - sl)
  rcr = dr * (sr - ur)
  
  ! Compute acoustic star state
  ustar = (rcr*ur   +rcl*ul   +  (pl-pr))/(rcr+rcl)
  pstar = (rcr*pl+rcl*pr+rcl*rcr*(ul-ur))/(rcr+rcl)
  
  ! Left star region variables
  dstarl = dl*(sl-ul)/(sl-ustar)
  estarl = ((sl-ul)*el-pl*ul+pstar*ustar)/(sl-ustar)
  
  ! Right star region variables
  dstarr = dr*(sr-ur)/(sr-ustar)
  estarr = ((sr-ur)*er-pr*ur+pstar*ustar)/(sr-ustar)
  
  ! Sample the solution at x/t=0
  if(sl > zero)then
      dd = dl
      uu = ul
      pp = pl
      ee = el
  elseif(ustar > zero)then
      dd = dstarl
      uu = ustar
      pp = pstar
      ee = estarl
  elseif(sr > zero)then
      dd = dstarr
      uu = ustar
      pp = pstar
      ee = estarr
  else
      dd = dr
      uu = ur
      pp = pr
      ee = er
  endif
  
  ! Compute the Godunov flux
  flux(1) = dd*uu
  flux(2) = dd*uu*uu + pp
  flux(5) = (ee + pp) * uu
  if(flux(1) > zero)then
      flux(3) = dd*uu*vl
      flux(4) = dd*uu*wl
  else
      flux(3) = dd*uu*vr
      flux(4) = dd*uu*wr
  endif

  return

end subroutine riemann_solver

!###############################################################################

function nmod(i,j)

  implicit none
  
  integer, intent(in) :: i,j
  integer             :: nmod
  
  nmod = mod(i,j)
  
  if(nmod == 0) nmod = j

end function nmod

!###############################################################################

function minmod(a,b)

  use constants
  
  implicit none
  
  real(dp), intent(in) :: a,b
  real(dp)             :: minmod
  
  if(abs(a) > abs(b))then
     minmod = b
  else
     minmod = a
  endif
  if(a*b < zero)then
     minmod = zero
  endif
  
end function minmod

!###############################################################################
